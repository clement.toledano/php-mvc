$(document).ready(function () {
    $('#buttonlogin').click(function () {

        // get form data

        var form_data = $('#login_form').serialize();
        //console.log(form_data);

        //http request will be here
        //submit form data to api
        $.ajax({
            method: "POST",
            url: "/account",
            data: form_data,
            success: function (result) {
                //console.log(result);
                //alert(result);
                if (result === 'Login success.') {
                    window.location.replace("/account");
                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>")
                }
            },
            // error response will be here
            error: function (xhr, resp, text) {
                console.log("ajax error - 404 not found");
                // on error, tell the user login has failed & empty the input boxes
                $('#response').html(
                    "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>"
                );
                //login_form.find('input').val('');
            }

        })
    })
    // function to set cookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    // serializeObject will be here
    // function to make form values to json format
    $.fn.serializeObject = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});
