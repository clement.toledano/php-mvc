$(document).ready(function () {
    $(".button_delete_car").click(function () {
        var carId = $(this).val();
            
        $.ajax({
            method: "DELETE",
            url: "/api/db_car/" + carId,
            success: function (result) {
                console.log(result);
                if (result === 'deleted') {
                    window.location.href = '/account_db_admin'

                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>Erreur de suppression</div>")
                }
            },
            // error response will be here
            error: function (xhr, resp, text) {
                console.log("ajax error - 404 not found");
                // on error, tell the user login has failed & empty the input boxes
                $('#response').html(
                    "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>"
                );
                //login_form.find('input').val('');
            }

        })
    })

})