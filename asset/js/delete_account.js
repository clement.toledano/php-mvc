$(document).ready(function () {
    $("#delete_account").click(function () {
        var userId = $('#userid').val();

        $.ajax({
            method: "DELETE",
            url: "/api/users/" + userId,
            success: function (result) {
                
                if (result === "1") {
                   window.location.href = "/delete_account";
                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>le compte n'existe pas.</div>")
                }
            },
            // error response will be here
            error: function (xhr, resp, text) {
                console.log("ajax error - 404 not found");
                // on error, tell the user login has failed & empty the input boxes
                $('#response').html(
                    "<div class='alert alert-danger'>Login failed. Email or password is incorrect.</div>"
                );
                //login_form.find('input').val('');
            }

        })
    })

})