$(document).ready(function () {
    $("#account").click(function () {
        
        $.get("/account", function (data, status) {
            if (status === "success") {
               // console.log(data);
                if (data === "false") {
                    $('#response').html(
                        "<div class='alert alert-danger'>Please login to access the Account page.</div>"
                    )
                } else {
                    window.location.replace("/account");
                }
            } else {
                console.log("account.js : ajax error - 404 not found");
            }
        });

    })
})