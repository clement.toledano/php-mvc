$(document).ready(function () {

    if (typeof $.cookie('tkn') === "undefined") {
        $("#login, #sign_up").show();
        $("#account, #logout").hide();
    } else {
        $("#login, #sign_up").hide();
        $("#account, #logout").show();
    }

});