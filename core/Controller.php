<?php

abstract class Controller
{
    /**
     * Cette propriété correspond a la variable superglobale $_GET, elle sera initialisée a la création d'un controlleur 
     * @var array
     */
    private $_get;

    /**
     * Cette propriété correspond a la variable superglobale $_POST, elle sera initialisée a la création d'un controlleur
     * @var array 
     */
    private $_post;

     /**
     * Cette propriété correspond a la variable superglobale $_POST, 
     * elle sera initialisée a la création d'un controlleur
     * 
     * REST : La methode put est utilisée pour mettre a jour une ressource
     * 
     * @var array 
     */

    private $_put;

    /**
     * Cette propriété sera initialisée lors de l'appel de la methode securityLoader()
     * @var SecurityMiddleware 
     */
    protected $_security;

    /**
     * Initialise le contexte de security.
     */
    protected function securityLoader()
    {
        $this->_security = new SecurityMiddleware();
    }

    /**
     * Le constructeur sera invoqué a la création des objets heritant de cette classe il initialisera les propriétés avec les valeurs des superglobales.
     */

    //Initialise les propriétés avec les valeurs des superglobales
    public function __construct() {
        $this->_get = $_GET;
        $this->_post = $_POST;
        parse_str(file_get_contents("php://input"), $this->_put);
    }


    /**
     * retourne la propriété $get afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     * 
     * @return array
     */
    protected function inputGet()
    {
        return $this->_get;
    }

    /**
     * retourne la propriété $post afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     * 
     * @return array
     */
    protected function inputPost()
    {
        return $this->_post;
    }

    /**
     * retourne la propriété $put afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     * 
     * @return array
     */
    protected function inputPut()
    {
        return $this->_put;
    }

    /**
     * La methode render afficher la vue selectionnée grace au premier argument
     * La methode utilise les indirection pour generer dynamiquement les noms des variables
     * utilisées dans la vue.
     * 
     * @param string $pathToView chemin du fichier de vue demandé
     * @param array $datas La valeur par defaut permet de retourner des vues statiques
     */
    final protected function render($pathToView,$datas=null) {
        $user = null;
        if(!is_null($this->_security)){
            $user = $this->_security->acceptConnexion();
            $user = (!$user)?null:$user;
        }        
        if(is_array($datas)){
            foreach ($datas as $key => $value) {
                $$key = $value;
            }
        }
        include './views/' . $pathToView . ".php";
    }

}
