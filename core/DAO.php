<?php




abstract class DAO implements RepositoryInterface, CRUDInterface
{

    public $_dbname;
    protected $_pdo;

    public function __construct()
    {
        // extrait les données du JSON
        $a = file_get_contents('///home/clement/ServeurWeb/php-mvc/config/database.json'); //lis le JSON
        $b = json_decode($a); //decode le JSON en tableau associatif(objet)

        $driver = $b->{'driver'};
        $host = $b->{'host'};
        $port = $b->{'port'};
        $dbname = $b->{'dbname'};
        $this->_dbname = $dbname;

        $dsn = $driver . ':dbname=' . $dbname . ';host=' . $host . ';port=' . $port;

        $username = $b->{'username'};
        $password = $b->{'password'};
        try {
            $this->_pdo = new PDO($dsn, $username, $password);
        } catch (PDOException $e) {
            echo 'Connexion échouée (PDO) : ' . $e->getMessage();
        }
    }

    // retourner la chaine de connexion de la base


    public function getAll()
    {

    }

    public function getAllBy($array)
    { }



/** --------------- CRUD -------------- */

    public function retrieve($id)
    { } 

    public function update($id)
    { } 

    public function delete(int $id) 
    { }

    public function create($array)
    { }
}
