<?php 

interface CRUDInterface{


    public function retrieve($id);//retourne une entité

    public function update($id);//retourne une entité

    public function delete(int $id);//retourne un booleen

    public function create($array);//retourne un booleen

}