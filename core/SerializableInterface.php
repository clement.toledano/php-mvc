<?php

interface SerializableInterface {

    public function serialize();
    public function unserialize($data);

}