<?php

class DAOUser extends DAO
{
    //*CRUDInterface

    public function retrieve($id)
    {
        $result = $this->_pdo->query("SELECT * FROM users WHERE id=" . $id);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
        // var_dump($res);
        return $res;
    }


    public function userConnexion(string $username)
    {

        $result = $this->_pdo->query("SELECT * FROM users WHERE username='" . $username . "'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
        //var_dump($res);
        return $res;
    } //* retourne une entité

    public function delete(int $id)
    {
        $req = "DELETE FROM users WHERE id=" . $id;
        $db = $this->_pdo;
        $stmt = $db->prepare($req);
        $stmt->execute();
        $reponse = ($stmt->execute()) ? true : false;
        echo $reponse;
    }


    public function create($array)
    {
        $array = $_POST;
        //var_dump($_POST);

        if ($this->exists($array['username']) === false) {
            $req = "INSERT INTO users (username, role, password) VALUES (:username, :role, :password)";
            $db = $this->_pdo;
            $stmt = $db->prepare($req);
            $stmt->bindParam(':username', $array['username'], PDO::PARAM_STR);
            $stmt->bindParam(':role', $array['role'], PDO::PARAM_STR);
            $stmt->bindParam(':password', $array['password'], PDO::PARAM_STR);
            echo ($stmt->execute()) ? "true" : "error";
        } else {
            echo "false";
        }
    }



    //*RepositoryInterface

    public function getAll()
    {
        $rows = [];
        $req = "SELECT * FROM users";
        $db = $this->_pdo;
        $stmt = $db->prepare($req);
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                array_push($rows, $row);
            }
        }
        //var_dump($rows);
        return $rows;
    }
    public function getAllBy($array)
    { } //* $array => tableau associatif pour les clauses WHERE et AND de la base SQL



    public function update($id)
    {
        // $id = $id[0];
        //var_dump($id);
        $getPut = (new ViewController())->getDatasFromPut();

        //var_dump($getPut);

        $req = " UPDATE users SET username = ?, role = ?, password = ? WHERE id =" . $id;
        $db = $this->_pdo;
        $stmt = $db->prepare($req);
        $res = (!$stmt->execute(array($getPut['username'], $getPut['role'], $getPut['password']))) ? 'error' : 'ok';
        echo $res;
    }

    public function exists($username)
    {
        $result = $this->_pdo->query("SELECT * FROM users WHERE username='" . $username . "'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
        return $res;
    }
}
