<?php


/**
 *
 * @author loic
 */
interface UserInterface {
    
    public function getUsername();
    public function getPassword();
    public function getRoles();
}
