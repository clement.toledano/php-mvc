<?php

abstract class EntityModel extends DAO implements PersistableInterface, SerializableInterface
{

    protected $_dao;

    //Methode load() invoque la méthode retrieve sur le dao en passant en argument l’id de l’entité courante.
    public function load($id)
    {
        $id = get_Class($this);
        $this->_dao->retrieve($id);
    }

    //Méthode update() invoque la méthode create() sur le dao si l’entité courante n’as pas d’id et update dans le cas contraire.
    public function update($id)
    {
        $id = get_Class($this);
        if ($this->id) {
            $this->_dao->create($id);
        } else {
            $this->_dao->update($id);
        }
    }

    //Méthode remove() invoque la méthode delete() du dao.
    public function remove($id)
    {
        $id = get_Class($this);
        $this->_dao->delete($id);
    }
    //Méthode qui retourne un tableau en chaine de caractère..
    public function serialize()
    {
        return serialize($this->_dao);
    }
    //Méthode 
    public function unserialize($_dao)
    {
        $this->_dao = unserialize($_dao);
    }
}
