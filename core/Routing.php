<?php

class Routing
{
    //*     //* PRORIETES
    public $_config; //*représentation  en  tableau  associatif  du  fichier routing.json
    public $_uri; //* résultat du découpage de l’URI en tableau
    public $_route; //* résultat du découpage de la route (clé de config) testée
    public $_controller; //* correspond au contrôleur qui a été trouvé
    public $_args; //* tableau des arguments à passer au contrôleur, qui représente les éléments variables de l’URI
    public $_method; //* correspond au verbe http utilisé lors de la requete



    //*     //* METHOD
    public function __construct()
    {
        //*recupère le contenu du fichier routing.json et $_config initialisée à la  création  de l’objet => tableau  associatif  du  fichier routing.json (option true de json_decode)
        $this->_config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/routing.json'), true);
        //* $_uri initialisée à la  création  de l’objet => tableau du resultat de $_SERVER['REQUEST_URI']
        $this->_uri = explode("/", $_SERVER['REQUEST_URI']);
        //* $_args est défini comme un tableau, vide à la construction de l'objet
        $this->_args = [];
    }
    // public function test()
    // {
    //    return $this->_config;
    // }

    //* execute() => invoquée à chaque requête http, cette méthode déclenche le mécanisme de routage

    public function execute()
    {
        //* si l'uri contient index.php, on le retire du tableau 
        // if ($this->_uri[1] === 'index.php') {
        //     array_splice($this->_uri, 1, 1);
        // }
        //?var_dump($this->_uri);
        //* appel la fonction sanitize pour $uri (enlève les 'vides' du tableau);
        $this->_uri = $this->sanitize($this->_uri, "");
        //? var_dump($this->_uri);
        //* chaque clé du tableau config, défini une route
        foreach ($this->_config as $key => $value) :
            //* scinde $key en segments au niveau du/des "/" => $route = tableau de chaînes de caractères
            $route = $this->sanitize(explode("/", $key), "");
        //? var_dump($route);
        //* verifie pour chaque route si les tableaux route et uri sont de même longueur
        if ($this->isEqual($route) === true) :
            //?  var_dump($route); 
            //* lorsqu'ils sont de même longueur, appel la method compare
            if ($this->compare($route) !== false) :
                //* si la route est trouvée, "met à jour" la propriété $_route de l'objet et appel la method getVAlue();
                //? var_dump($route);
                $this->_route = $route;
                $this->getVAlue($value);
                //* appel la method invoke()
                $this->invoke();
            endif;
        endif;
        endforeach;
    }


    //* isEqual() => retourne un booléen correspondant au résultat de la comparaison des hauteurs des deux tableaux

    public function isEqual($route)
    {
        //* verifie la longueur des tableaux route et uri
        if (count($route) === count($this->_uri)) :
            //* retourne true si ils sont de même longueur
            return true;
        endif;
    }


    //* getValue() => retourne le contrôleur correspondant à la route sélectionnée

    public function getVAlue($value)

    {
        //* si $value est un string => on récupère le controller et la method directement
        if (gettype($value) === 'string') :
            $this->_controller = $value;
        //? var_dump($this->_controller);
        //* sinon $value est un tableau et il faut vérifier le verbe http qui correspond a la clé du tableau $value
        else :
            //* recupère le verbe http dans une variable et met à jour la propriété de l'objet
            $this->_method = $_SERVER['REQUEST_METHOD'];
            //? var_dump($->_method);
            //? var_dump($value);
            //* pour chaque clé de $value on vérifie si la clé correspond au verbe http
            foreach ($value as $key => $val) {
                if ($key === $this->_method) :
                    $this->_controller =  $val;
                    if($key === 'POST'):
                        $this->_args = $_POST;
                        //? var_dump($this->_args);
                    endif;
                //? var_dump($this->_controller);
                endif;
            }
        endif;
    }


    //* addArgument() => ajoute l’élément variable  de  l’URI  si l’élémenten cours est censé être variable

    public function addArgument($i)
    {
        //* stock la valeur de l'uri à l'index $i (élement de variable) dans le tableau des arguments
        array_push($this->_args, $this->_uri[$i]);
        //var_dump($this->_args);
    }

    //* compare() compare les éléments des deux tableaux (uri et route) si les deux tableaux correspondent, c’est que la route a été trouvée

    public function compare($route)
    {
        //? var_dump($route);
        //* pour chaque index des tableaux route et uri
        for ($i = 0; $i < count($route); $i++) {
            //* verifie si les valeurs sont différentes
            if ($route[$i] != $this->_uri[$i]) :
                //* si elles sont différentes, verifie si $route[$i] est un élement de variable
                if ($route[$i] !== null && strpos($route[$i], "(:)") !== false) :
                    //?print $index;
                    //* appel la method addArgument
                    $this->addArgument($i);
                //*si ce n'est pas un élement de variable, retourne false
                else : return false;
                endif;
            endif;
            //* si la method ne retourne pas false, la route a été trouvée !!
        }
    }

    //* méthode invoke() invoquée quand le contrôleur a été sélectionnée. Elle créée un objet contrôleur et invoque la méthode en y passant les arguments adéquats

    public function invoke()
    {
        $controller = explode(":", $this->_controller);
        $controllerMethod = $controller[1];
        //? var_dump($controller);
        //* instancie un nouvel objet $c selon la valeur de $controleur
        $c = new $controller[0]();
       //? var_dump($c);
       //var_dump($this->_args);
        //* appel la method défini par $controller[1] avec les bons arguments (vide ou non) pour l'objet $c
       call_user_func_array(array($c, $controller[1]), $this->_args);
     //   $c->$controllerMethod($this->_args);
    }

    //* sanitize() => nettoie les listes $uri et $route en enlevant les éléments qui sont une chaine vide

    public function sanitize($array, $var)
    {
        //? var_dump($array);
        //* Pour chaque valeur du tableau, si elle est egale à "", alors on l'enlève du tableau et décale les index
        foreach ($array as $key => $value) {
            if ($value === $var) :
                array_splice($array, $key, 1);
            endif;
        }
       //? var_dump($array);
        //* la fonction retourne la tableau sans vide et avec les index commencant à 0
        return $array;
    }
}
