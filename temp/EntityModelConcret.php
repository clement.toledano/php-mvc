<?php

class EntityModelConcret extends EntityModel
{
    protected $_dao;


    //automatise la création du dao
    // public function __construct()
    // {
    //     $this->_dao = $this->get_pdo();
    // }

    //Methode load() invoque la méthode retrieve sur le dao en passant en argument l’id de l’entité courante.
    public function load($id)
    {
        $this->_dao->retrieve($id);
        
    }
}