<?php include "header.php"; ?>

<div class="row">
    <?php include "account_side.php"; ?>
    
    <div role="main" class="container starter-template col-lg-9">
            <input type="hidden" id="userId" value="<?php echo $datas[1]['id']; ?>">
        
        <div class="row">
            
            <div class="col">
                
                <!-- where prompt / messages will appear -->
                <div id="response"></div>
                
            </div>
        </div>
        
        </html>
        <h1>Account update</h1>
        
        <form id='update_form'>
            <div class="form-group">
                <label for="username">Entrez votre nouveau login :</label>
                <input type="text" class="form-control" value="<?php echo $datas[1]['username']; ?>" name="username" id="username" required />
            </div>
            <div class="form-group">
                <?php if ($datas[1]['role'] === "admin") {
                    echo
                        '<label for="role">Choisissez un role :</label>
                    <label>Admin
                        <input type="radio" name="role" value="admin" checked>
                    </label>
                    <label>Visiteur
                        <input type="radio" name="role" value="visiteur">
                    </label>';
                } else {
                    echo
                        '<label for="role">Choisissez un role :</label>
                    <label>Admin
                        <input type="radio" name="role" value="admin">
                    </label>
                    <label>Visiteur
                        <input type="radio" name="role" value="visiteur" checked>
                    </label>';
                } ?>
            </div>
            <div class="form-group">
                <label for="password">Enter your password :</label>
                <input type="text" class="form-control" value="<?php echo $datas[1]['password']; ?>" name="password" id="password" required />
            </div>
            <button type='button' id='buttonUpdate' class='btn btn-primary'>Update</button>
        </form>


        <table class="table">
        </table>
    </div>
</div>

<?php include "footer.php"; ?>