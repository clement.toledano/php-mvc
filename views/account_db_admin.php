<?php include "header.php"; ?>
<!-- container -->
<div class="row">
    <?php include "account_side.php"; ?>
    <div role="main" class="container starter-template col-lg-9">

        <div class="row">

            <div class="col">

                <!-- where prompt / messages will appear -->
                <div id="response"></div>


            </div>
        </div>

        </html>
        <h1>Administration de la base de donnée </h1>

        <table class="table">
            <tr>
                <form id="db_add_car">
                    <td>
                        <button type="button" class="button_add_car btn btn-dark">Add</button>
                    </td>
                    <td><input type="text" name="brand" id="brand"></td>
                    <td><input type="text" name="vehicle" id="vehicle"></td>
                </form>
            </tr>
            <?php
            /* Debut de la zone qui nous interesse */
            foreach ($datas as $car) : ?>
                <tr>
                    <td>
                        <form>
                            <button type="button" value="<?= $car['id']; ?>" class="button_delete_car btn btn-dark">Delete</button>
                        </form>
                    </td>
                    <td id="carIdx"><?= $car['id']; ?></td>
                    <td><?= $car['brand']; ?></td>
                    <td><?= $car['vehicle']; ?></td>
                </tr>
                <?php
                endforeach; ?>
                
            </table>
        </div>
    </div>
    <!-- /container -->
    <?php include "footer.php"; ?>