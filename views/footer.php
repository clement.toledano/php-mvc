        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="valid_logout">Logout</a>
              </div>
            </div>
          </div>
        </div>


        <!-- Logout Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to delete?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Are you sure you want to delete this account?</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="delete_account" >Delete Account</a>
              </div>
            </div>
          </div>

          <!-- jQuery & Bootstrap 4 JavaScript libraries -->
          <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
          <script src="asset/js/jquery.cookie.js"></script>

          <script src="asset/js/index.js"></script>
          <script src="asset/js/menu.js"></script>
          <script src="asset/js/delete_db_car.js"></script>
          <script src="asset/js/create_db_car"></script>
          <script src="asset/js/login.js"></script>
          <script src="asset/js/logout.js"></script>
          <script src="asset/js/account.js"></script>
          <script src="asset/js/create_account.js"></script>
          <script src="asset/js/update_account.js"></script>
          <script src="asset/js/delete_account.js"></script>

          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
          </script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
          </script>

          </body>

          </html>