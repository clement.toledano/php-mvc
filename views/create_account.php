<?php include "header.php"; ?>
<!-- container -->
<main role="main" class="container starter-template">

    <div class="row">
        <div class="col">

            <!-- where prompt / messages will appear -->
            <div id="response"></div>

            <!-- where main content will appear -->
            <div id="content"></div>
            <h2>Sign Up - Create an accout !</h2>
            <form id='sign_up_form'>
                <div class="form-group">
                    <label for="username">Entrez votre login :</label>
                    <input type="text" class="form-control" value="aze" name="username" id="username" required />
                </div>
                <div class="form-group">
                    <label for="role">Choisissez un role :</label>
                    <label>Admin
                        <input type="radio" name="role" value="admin" checked>
                    </label>
                    <label>Visiteur
                        <input type="radio" name="role" value="visiteur">
                    </label>
                </div>
                <div class="form-group">
                    <label for="password">Enter your password :</label>
                    <input type="text" class="form-control" value="aze" name="password" id="password" required />
                </div>
                <button type='button' id='buttoncreate' class='btn btn-primary'>Sign Up</button>
            </form>
        </div>
    </div>

</main>
<!-- /container -->

<?php include "footer.php"; ?>