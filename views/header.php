<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <title>Rest API Authentication Example</title>

    <!-- CSS links will be here -->
    <!-- Bootstrap 4 CSS and custom CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="custom.css" />

</head>

<body>

    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="/">FRAMEWORK BEWEB </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="/" id='home'>Home</a>
                <a class="nav-item nav-link" href="#" id='account'>Account</a>
                <a class="nav-item nav-link" href="logout" data-toggle="modal" data-target="#logoutModal" id='logout'>Logout</a>

                <a class="nav-item nav-link" href="login" id='login'>Login</a>
                <a class="nav-item nav-link" href="/create_account" id='sign_up'>Sign Up</a>
            </div>
        </div>
    </nav>

    

    <div id="response">

        <?php
        if (isset($_SESSION['msg'])) {
            echo "<div class='alert alert-primary'>" . $_SESSION['msg'] . "</div>";
            unset($_SESSION["msg"]);
        }
        //var_dump($_COOKIE);
        ?>
    </div>

    <!-- /navbar -->