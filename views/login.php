<?php include "header.php"; ?>
<!-- container -->
<main role="main" class="container starter-template">

    <div class="row">
        <div class="col">
            <h2>Login</h2>
            <!-- where prompt / messages will appear -->
            <div id="response"></div>

            <!-- where main content will appear -->
            <div id="content"></div>

            <form id="login_form">
                <div class="form-group">
                    <label for="username">Your Username :</label>
                    <input type="text" class="form-control" name="username" id="username" value="aze" required />
                </div>
                <div class="form-group">
                    <label for="vehicle">Your Password :</label>
                    <input type="text" class="form-control" name="password" id="password" value="aze" required />
                </div>

                <button type="button" id="buttonlogin" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>

</main>
<!-- /container -->

<?php include "footer.php"; ?>