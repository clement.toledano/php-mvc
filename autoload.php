<?php
spl_autoload_register(function ($class) {
    if (file_exists('./controllers/' . $class . '.php')) :
        include './controllers/' . $class . '.php';
    elseif (file_exists('./dao/' . $class . '.php')) :
        include './dao/' . $class . '.php';
    elseif (file_exists('./routing/' . $class . '.php')) :
        include './routing/' . $class . '.php';
    elseif (file_exists('./models/' . $class . '.php')) :
        include './models/' . $class . '.php';
    elseif (file_exists('./core/' . $class . '.php')) :
        include './core/' . $class . '.php';
    elseif (file_exists('./config/' . $class . '.php')) :
        include './config/' . $class . '.php';
    elseif (file_exists('./vendors/' . $class . '.php')) :
        include './vendors/' . $class . '.php';
    elseif (file_exists('./views/' . $class . '.php')) :
        include './views/' . $class . '.php';
    endif;
});
