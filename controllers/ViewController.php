<?php

class ViewController extends Controller
{
    public function getView()
    { }

    public function getDatasFromPOST()
    {
        return $this->inputPost();
    }

    public function getDatasFromPUT()
    {
        return $this->inputPut();
    }

    public function getCreate()
    {
        $this->render("create_account");
    }
    public function getLogin()
    {
        $this->render("login");
    }

 
    public function getHome()
    {
        $datas = (new DAOCar())->getAll();
        $this->_datas = $datas;
        $this->render("index", $datas);
    }

    public function getAccount()
    {
        if (!empty($_COOKIE['tkn'])) {
            $security = new SecurityMiddleware;
            $tkn = $security->acceptConnexion();
            $datas = $tkn;

            $this->render("/account_index", $datas);
        } else {

            echo 'false';
        }
    }

    public function getUpdateAccount()
    {
        if (!empty($_COOKIE)) {
            $security = new SecurityMiddleware;
            $tkn = $security->acceptConnexion();

            $data = (new DAOUser())->retrieve($tkn->id);
            $datas = array($tkn, $data);
            $this->render("/account_update", $datas);
        } else {
            echo 'false';
        }
    }

    public function getDeleteAccount()
    {
        if (!empty($_COOKIE)) {
            $getOut = new SecurityMiddleware;
            $getOut->desactivate();
            $_SESSION['msg'] = "Compte supprimé avec succés !";
            header('Location: /');
            
        } else {

            header('Location: /');
        }
    }

    public function getAdminDatabase()
    {
        if (!empty($_COOKIE)) {
            $security = new SecurityMiddleware;
            $tkn = $security->acceptConnexion();

            $data = (new DAOUser())->getAll();
            $datas = array($tkn, $data);

            $this->render("/account_admindb", $datas);
        } else {

            echo 'false';
        }
    }
    public function getLogout()
    {
        $getOut = new SecurityMiddleware;
        $getOut->desactivate();

        $_SESSION['msg'] = "Vous etes deconnecté !";
        
        header('Location: /');
        
    }

  
    /**
     * account_db_admin
     *
     * @return void
     */
    public function account_db_admin()
    {
        $datas = (new DAOCar())->getAll();
        $this->_datas = $datas;
        $this->render("account_db_admin", $datas);
    }
}
