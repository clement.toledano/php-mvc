<?php

class UserController extends Controller implements UserInterface
{
    protected $_user;
    protected $_username;
    protected $_role;
    protected $_userId;
    protected $_password;
    protected $_userPost;

    public function __construct()
    {
        parent::__construct();

    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function getRoles()
    {
        return $this->_role;
    }
    public function getUserId()
    {
        return $this->_userId;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function getToken()
    {
        $this->_userPost = $this->inputPost();
 
        $_userConnexion = new DAOUser;
        $this->_user = $_userConnexion->userConnexion($this->_userPost['username']);
        //$this->securityLoader();
        
        $this->_username = $this->_user['username'];
        $this->_password = $this->_user['password'];
        $this->_role = $this->_user['role'];
        $this->_userId = $this->_user['id'];
        if ($this->_userPost['username'] === $this->_username && $this->_userPost['password'] === $this->_password) {


            $security = new SecurityMiddleware;
            $security->generateToken($this);
            $_SESSION['msg'] = "Vous etes connecté !";

            echo 'Login success.';
        } else {
            echo 'utilisateur ou mot de passe incorrecte';
         

        }
    }


}
